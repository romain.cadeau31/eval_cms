<?php
/*
Plugin Name: My Plugin modif
Plugin URI: http://me.me/
Description: plugin permettra de modifier l'annuaire
Author: moi
Version: 1.0
Author URI: http://me.me/
*/

function my_admin_menu() {
    add_menu_page(
        __( 'Update', 'my-project' ), // Page Title 
        __( 'Update', 'my-project' ), // Menu Title
        'manage_options', // Capability
        'update', // Menu Slug
        'shortcode_modifannuaire', // Page Callback function
        'dashicons-schedule', // Dashicon
        3 // Menu Position
    );
}
add_action( 'admin_menu', 'my_admin_menu' );


function shortcode_modifannuaire(){

    global $wpdb; // On se connecte à la base de données du site
    $entreprises = $wpdb->get_results("
    SELECT id, nom_entreprise,
    localisation_entreprise,
    prenom_contact,
    nom_contact,
    mail_contact
    FROM wp_annuaire;
    ");
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <!-- Insérer cette balise "link" après celle de Bootstrap -->
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">

<!-- Insérer cette balise "script" après celle de Bootstrap -->
<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>

<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/locale/bootstrap-table-fr-FR.min.js"></script>
</head>

<body>

<div class="row">
    <h2 class="m-auto">Ajouter une entreprise</h2>
</div>

<div class="formAjout">
    <form method="POST"  action="">
        <div class="row">
            <div class="form-group mt-5 ml-5">
                <label for="nom_entreprise">Nom entreprise</label>
                <input type="text" class="form-control" name="nom_entreprise" placeholder="Nom entreprise">
            </div>
            <div class="form-group mt-5 ml-5">
                <label for="localisation_entreprise">Localisation</label>
                <input type="text" class="form-control" name="localisation_entreprise" placeholder="Localisation">
            </div>
            <div class="form-group mt-5 ml-5">
                <label for="prenom_contact">Prénom</label>
                <input type="text" class="form-control" name="prenom_contact" placeholder="Prénom">
            </div>
            <div class="form-group mt-5 ml-5">
                <label for="nom_contact">Nom</label>
                <input type="text" class="form-control" name="nom_contact" placeholder="Nom">
            </div>
            <div class="form-group mt-5 ml-5">
                <label for="mail_contact">Mail</label>
                <input type="text" class="form-control" name="mail_contact" placeholder="Mail">
            </div>
        </div>
        <div class="row">
            <button type="submit" class="btn btn-primary m-auto" name="envoyer">Envoyer</button>
        </div>
    </form>
</div>

<div class="row mt-3">
    <h2 class="m-auto">Modifier une entreprise</h2>
</div>

<div class="row mt-3">
            <div class="col-12">
                <table class="table table-hover table-dark" data-toggle="table" data-search="true" data-pagination="true">
                    <thead>
                        <tr>
                            <th data-sortable="true" data-field="Nom entreprise">Nom entreprise</th>
                            <th data-sortable="true" data-field="Localisation">Localisation</th>
                            <th data-sortable="true" data-field="Prénom">Prénom</th>
                            <th data-sortable="true" data-field="Nom">Nom</th>
                            <th data-sortable="true" data-field="Mail">Mail</th>
                            <th data-field="Action">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // On boucle sur tous les articles
                        foreach($entreprises as $entreprise){
                        ?>
                            <tr>
                                <td><?= $entreprise->nom_entreprise ?></td>
                                <td><?= $entreprise->localisation_entreprise ?></td>
                                <td><?= $entreprise->prenom_contact ?></td>
                                <td><?= $entreprise->nom_contact ?></td>
                                <td><?= $entreprise->mail_contact ?></td>
                                <td>
                                    <form method="post">
                                        <button type="button" class="btn btn-sm btn-warning"  data-toggle="modal" data-target="#<?php echo $entreprise->nom_entreprise;?>" ><i class="fa fa-edit"></i></button>
                                        <button type="submit" class="btn btn-sm btn-danger"  name="supprimer" value="<?php echo $entreprise->id; ?>"><i class="fa fa-trash"></i></button>
                                    </form>

                                     <!-- Modal -->
                                     <div class="modal fade" id="<?php echo $entreprise->nom_entreprise;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <section class='container'>
                                                    <form method="post" action=''>
                                                        <div class="form-group">
                                                            <label for="updateNameEnt">Nom entreprise</label>
                                                            <input type="text" name='NomEntreprise' class='form-control' id='exampleInputName' value="<?php echo $entreprise->nom_entreprise;?>">                                                        
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputLocalisation">Localisation</label>
                                                            <input type="text" name='LocalisationEntreprise' class="form-control" id="exampleInputLocalisation" aria-describedby="LocalisationHelp" value="<?php echo $entreprise->localisation_entreprise;?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputFirstNameContact">Prénom contact</label>
                                                            <input type="text" name='PrenomContact' class="form-control" id="exampleInputFirstNameContact" aria-describedby="firstNameContactHelp" value="<?php echo $entreprise->prenom_contact;?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Nom contact</label>
                                                            <input name='NomContact' class="form-control" id="exampleInputEmail1" aria-describedby="namelHelp" value="<?php echo $entreprise->nom_contact;?>">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Email address</label>
                                                            <input name='EmailContact' type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="<?php echo $entreprise->mail_contact;?>">
                                                        </div>
                                                        <button type="submit" name='modifier' value="<?php echo $entreprise->id; ?>" class="btn btn-primary">Modifier</button>
                                                    </form>
                                                </section>
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                        <?php
                        };
                        ?>
                    </tbody>
                </table>
            </div>
        </div>



<?php

    @$nom_entreprise = $_POST['nom_entreprise'];
    @$localisation_entreprise = $_POST['localisation_entreprise'];
    @$prenom_contact = $_POST['prenom_contact'];
    @$nom_contact = $_POST['nom_contact'];
    @$mail_contact = $_POST['mail_contact'];
    @$envoyer = $_POST['envoyer'];

    @$NomEntreprise = $_POST['NomEntreprise'];
    @$LocalisationEntreprise = $_POST['LocalisationEntreprise'];
    @$PrenomContact = $_POST['PrenomContact'];
    @$NomContact = $_POST['NomContact'];
    @$EmailContact = $_POST['EmailContact'];


    @$supprimer = $_POST['supprimer'];
    @$modifier = $_POST['modifier'];

    if(isset($supprimer)){
        $wpdb->delete('wp_annuaire', array( 'id' => $supprimer ) );
        echo '<script>parent.window.location.reload(true);</script>';
    }

    if(isset($modifier)){
        $wpdb->update('wp_annuaire', 
        array('nom_entreprise'=>$NomEntreprise, 
        'localisation_entreprise'=>$LocalisationEntreprise,
        'prenom_contact'=>$PrenomContact,
        'nom_contact'=>$NomContact,
        'mail_contact'=>$EmailContact
            ), array('id' => $modifier));
        echo '<script>parent.window.location.reload(true);</script>';
    }
    

    if(isset($envoyer)){
        global $wpdb;
        $wpdb->insert('wp_annuaire', 
        array('nom_entreprise'=>$nom_entreprise, 
            'localisation_entreprise'=>$localisation_entreprise,
            'prenom_contact'=>$prenom_contact,
            'nom_contact'=>$nom_contact,
            'mail_contact'=>$mail_contact
    ));
    echo '<script>parent.window.location.reload(true);</script>';
}
?>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>

<?php
}


add_shortcode('modifannuaire', 'shortcode_modifannuaire');