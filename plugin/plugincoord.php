<?php
/*
Plugin Name: My Plugin
Plugin URI: http://me.me/
Description: plugin permettra d'afficher dans une page une liste de coordonnées tirées d'une table
Author: moi
Version: 1.0
Author URI: http://me.me/
*/

function shortcode_annuaire(){

    global $wpdb; // On se connecte à la base de données du site
    $entreprises = $wpdb->get_results("
    SELECT nom_entreprise,
    localisation_entreprise,
    prenom_contact,
    nom_contact,
    mail_contact
    FROM wp_annuaire;
    ");
    // print_r($entreprises);
    
    ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <!-- Insérer cette balise "link" après celle de Bootstrap -->
<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.css">

<!-- Insérer cette balise "script" après celle de Bootstrap -->
<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/bootstrap-table.min.js"></script>

<script src="https://unpkg.com/bootstrap-table@1.16.0/dist/locale/bootstrap-table-fr-FR.min.js"></script>
</head>

<body>
    <section class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-hover table-dark" data-toggle="table" data-search="true" data-pagination="true">
                    <thead>
                        <tr>
                            <th data-sortable="true" data-field="Nom entreprise">Nom entreprise</th>
                            <th data-sortable="true" data-field="Localisation">Localisation</th>
                            <th data-sortable="true" data-field="Prénom">Prénom</th>
                            <th data-sortable="true" data-field="Nom">Nom</th>
                            <th data-sortable="true" data-field="Mail">Mail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // On boucle sur tous les articles
                        foreach($entreprises as $entreprise){
                        ?>
                            <tr>
                                <td><?= $entreprise->nom_entreprise ?></td>
                                <td><?= $entreprise->localisation_entreprise ?></td>
                                <td><?= $entreprise->prenom_contact ?></td>
                                <td><?= $entreprise->nom_contact ?></td>
                                <td><?= $entreprise->mail_contact ?></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>

<?php

}
add_shortcode('annuaire', 'shortcode_annuaire');


